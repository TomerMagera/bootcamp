# ASP.NET web application hostes on IIS web server

This sample is meant to demonstrate how IIS is used to host web sites/pages.

## Creating and building the web application

* We added Newtonsoft.json package to the solution/project in VS.
1. Tools-->Nuget Package Manager-->Manage Nuget Packages for Solution
2. In 'Package source:', I needed to set the source URL of nuget.org to be https://api.nuget.org/v3/index.json.
3. Then searched for Newtonsoft.json and selected version 13.0.1 and clicked Install.
* Then in the solution explorer, I right click the solution and chose 'Restore Nuget Packages', 
in order to update the solution/project dependencies.
* As a result I could see in project dependencies, under packages, the the package Newtonsoft.json.dll was added.
* Next I published the project in order to get it built, having the binaries created under publish directory,
including the Newtonsoft.json.dll.
* Then I copied the content of the publish folder into the C:\inetpub\wwwroot\Bootcamp.

## Istalling Internet Information Services (IIS) Manager

* Installed IIS from via Windows Features.
* Created BootcampAP application pool:
1. .NET CLR version: No Managed Code
2. Managed pipeline mode: Integrated

* Created a new site and associated it to the BootcampAP appication pool I created.
1. Called the site Bootcamp.
2. Port: 5100
3. Physical Path C:\inetpub\wwwroot\Bootcamp (where we copy the artifacts from the publish action in VS)
* The application pool should be stopped before copying published contents into the physical path, 
and started once done.
* When tried to view the page, I came across the following issue:

**ASP.NET: HTTP Error 500.19 : Internal Server Error 0x8007000d**

* The error message was not conclusive, and was actually confusing.
* After googling quite alot, and trying several things, I finally managed to resolve it.
* Via the VS Installer, I updated the VS installation with 'Development time IIS support' (some additional checkbox in the Optional part)
* I assume this also included something for runtime on the windows server which resolved the issue.
* Later on I removed 'Development time IIS support' with the VS installer, and 
installed 'ASP.NET Core 5.0 Runtime (v5.0.10) - Windows Hosting Bundle Installer!
* This helped also to resolve the error.

